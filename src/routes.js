import React from 'react'
import { Text } from 'react-native'

import { createAppContainer } from 'react-navigation'
import { createStackNavigator } from 'react-navigation-stack'

import RegisterPet from './pages/pet/RegisterPet'
import RegisterUser from './pages/user/RegisterUser'
import LoginUser from './pages/user/LoginUser'
import ListPet from './pages/pet/ListPet'

export default createAppContainer(
   createStackNavigator({
      LoginUser,
      ListPet,
      RegisterUser,
      RegisterPet
   }, {
      defaultNavigationOptions: {
         headerTitle: <Text style={{paddingLeft: 35, fontSize: 20, color: '#000F60'}}>The Vet</Text>,
         headerBackTitle: null
      },
      mode: 'modal'
   })
);
import React from 'react';
import { YellowBox } from 'react-native'
import Routes from './routes';

YellowBox.ignoreWarnings([
  'Warning: componentWillReceiveProps has been renamed, and is not recommended for use',
  'Warning: componentWillUpdate has been renamed, and is not recommended for use',
  'Warning: DatePickerAndroid has been merged',
  'Warning: Failed prop type',
]);

export default function App() {
  return <Routes />
}
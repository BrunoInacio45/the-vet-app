import React, { Component } from 'react';
import { View, Text, StyleSheet, TextInput, TouchableOpacity, Picker, Image } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import { NavigationEvents } from 'react-navigation';

import { Dropdown } from 'react-native-material-dropdown';
import DatePicker from 'react-native-datepicker'
import { CheckBox } from 'react-native-elements'

import api from '../../services/api';
import { FlatList } from 'react-native-gesture-handler';

export default class ListPet extends Component {

   state = {
      pets: [],
      user: ''
   }

   static navigationOptions = ({ navigation }) => {
      return {
         header: () => null
      }
   }

   registerPet = () => {
      this.props.navigation.navigate('RegisterPet');
   }

   async getPets(){
      this.setState({user: JSON.parse(await AsyncStorage.getItem('@storage_Key')).code})
      console.log(this.state.user)
      const response = await api.get('api/Pets/petswithuser?userId=' + this.state.user);
      this.setState({ pets: response.data })
   }

   async componentDidMount() {
      this.getPets();
   }

   render() {
      return (
         <View style={styles.container}>
            <NavigationEvents
               onWillFocus={() => this.getPets()}
               
            />
            <FlatList
               data={this.state.pets}
               renderItem={({ item }) => (
                  <View style={styles.card}>
                     <View style={styles.align}>
                        <View style={styles.img}>
                           <Image
                              style={styles.img}
                              source={require('./../../../assets/dog.jpg')}
                           />
                        </View>
                        <View style={styles.description}>
                           <Text style={styles.textDescription}>{item.name}</Text>
                        </View>
                     </View>
                  </View>
               )}
            />
            <View>
               <TouchableOpacity style={styles.shareButton} onPress={this.registerPet}>
                  <Text>Cadastrar Pet</Text>
               </TouchableOpacity>
            </View>
         </View>
      )
   }
}

const styles = StyleSheet.create({
   container: {
      flex: 1,
      paddingHorizontal: 40,
      paddingTop: 30,
      marginTop: 5,
      backgroundColor: '#fff'
   },
   card: {
      backgroundColor: '#f2f2f2',
      borderColor: '#bfbfbf',
      borderWidth: 1,
      borderRadius: 6,
      height: 90,
      marginBottom: 8
   },
   align: {
      flex: 1,
      flexDirection: "row",
      justifyContent: 'space-between'
   },
   description: {
      width: 196,
      backgroundColor: 'powderblue'
   },
   textDescription: {
      textAlign: 'center',
      fontSize: 18,
      color: 'gray',
      marginTop: 25
   },
   img: {
      height: 90,
      maxWidth: 115,
      borderRadius: 6,

   },
   shareButton: {
      backgroundColor: '#4cb69f',
      borderRadius: 15,
      height: 55,
      marginTop: 15,

      justifyContent: 'center',
      alignItems: 'center',
   }
});
import React, { Component } from 'react';
import { View, Text, StyleSheet, TextInput, TouchableOpacity } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';


import api from '../../services/api';

export default class RegisterPet extends Component {

   state = {
      name: '',
      birthDate: '',
      specie: '',
      breed: '',
      physicalSape: '',
      gender: '',
      image: '',
      users: []
   }

   static navigationOptions = ({ navigation }) => {
      return {
         headerLeft: () => null
      }
   }

   async componentDidMount() {
      try {
         const listUser = this.state.users;
         listUser.push(JSON.parse(await AsyncStorage.getItem('@storage_Key')).code);
         console.log(listUser);
         this.setState({ user: listUser });
      } catch (error) { }
   }

   handleSubmit = event => {
      api.post('api/pets', {
         name: this.state.name,
         birthDay: '2019-04-20',
         species: this.state.specie,
         physicalSape: this.state.breed,
         breed: this.state.physicalSape,
         gender: 1,
         users: this.state.user
      }).then(res => {
         this.props.navigation.navigate('ListPet');
      }).catch(err => {
         console.log(err);
      })
   }

   render() {
      return (
         <View style={styles.container}>
            <TextInput
               style={styles.input}
               autoCorrect={false}
               placeholder="Nome do Pet"
               placeholderTextColor="#999"
               value={this.state.name}
               onChangeText={name => this.setState({ name })}
            />
            <TextInput
               style={styles.input}
               autoCorrect={false}
               placeholder="Nascimento"
               placeholderTextColor="#999"
               value={this.state.birthDate}
               autoCapitalize = 'none'
               onChangeText={birthDate => this.setState({ birthDate })}
            />

            <TextInput
               style={styles.input}
               autoCorrect={false}
               placeholder="Espécie"
               placeholderTextColor="#999"
               value={this.state.specie}
               onChangeText={specie => this.setState({ specie })}
            />

            <TextInput
               style={styles.input}
               autoCorrect={false}
               placeholder="Raça"
               placeholderTextColor="#999"
               value={this.state.breed}
               onChangeText={breed => this.setState({ breed })}
            />

            <TextInput
               style={styles.input}
               autoCorrect={false}
               placeholder="Porte"
               placeholderTextColor="#999"
               value={this.state.physicalSape}
               onChangeText={physicalSape => this.setState({ physicalSape })}
            />

            <TouchableOpacity style={styles.shareButton} onPress={this.handleSubmit}>
               <Text style={styles.sharebuttonText}>Cadastrar</Text>
            </TouchableOpacity>
         </View>
      )

   }
}

const styles = StyleSheet.create({
   container: {
      flex: 1,
      paddingHorizontal: 20,
      paddingTop: 30,
   },

   input: {
      borderRadius: 4,
      borderWidth: 1,
      borderColor: '#ddd',
      padding: 15,
      marginTop: 10,
      fontSize: 16,
   },

   shareButton: {
      backgroundColor: '#4cb69f',
      borderRadius: 4,
      height: 42,
      marginTop: 15,

      justifyContent: 'center',
      alignItems: 'center',
   },

   shareButtonText: {
      fontWeight: 'bold',
      fontSize: 16,
      color: '#FFF',
   },
});

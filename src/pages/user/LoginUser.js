import React, { Component } from 'react';
import { View, Text, StyleSheet, TextInput, TouchableOpacity, ImageBackground } from 'react-native';
import AwesomeAlert from 'react-native-awesome-alerts';
import AsyncStorage from '@react-native-community/async-storage';

import api from '../../services/api';

export default class RegisterUser extends Component {
   state = {
      email: '',
      pass: '',
      showAlert: false
   }

   static navigationOptions = ({ navigation }) => {
      return {
         header: () => null
      }
   }

   showAlert = () => {
      this.setState({
         showAlert: true
      });
   };

   hideAlert = () => {
      this.setState({
         showAlert: false
      });
   };

   setStorage = async (data) => {
      try {
         console.log(data.code);
         await AsyncStorage.setItem('@storage_Key', JSON.stringify(data))
      } catch (error) { }
   }

   handleSubmit = () => {
      api.post('api/user/login', {
         username: this.state.email,
         password: this.state.pass
      }).then(res => {
         this.setStorage(res.data)
         this.props.navigation.navigate('ListPet');
      }, err => {
         this.showAlert();
      })
   }

   register = () => {
      this.props.navigation.navigate('RegisterUser');
   }

   render() {

      const { showAlert } = this.state;

      return (
         <ImageBackground
            style={styles.imgBackground}
            source={require('./../../../assets/backgroundLogin.jpg')}>

            <View style={styles.container}>
               <Text style={styles.title}>The Vet</Text>
               <TextInput
                  style={styles.input}
                  autoCorrect={false}
                  placeholder="E-mail"
                  placeholderTextColor="#999"
                  value={this.state.email}
                  autoCapitalize = 'none'
                  onChangeText={email => this.setState({ email })}
               />

               <TextInput
                  style={styles.input}
                  autoCorrect={false}
                  placeholder="Senha"
                  secureTextEntry={true}
                  placeholderTextColor="#999"
                  value={this.state.pass}
                  autoCapitalize = 'none'
                  onChangeText={pass => this.setState({ pass })}
               />

               <TouchableOpacity style={styles.shareButton} onPress={this.handleSubmit}>
                  <Text>Login</Text>
               </TouchableOpacity>

               <Text style={styles.subButton} onPress={this.register}>Não possuo cadastro</Text>

            </View>

            <AwesomeAlert
               show={showAlert}
               showProgress={false}
               title="Atenção"
               message="Parece que um ou mais dados estão inválidos"
               closeOnTouchOutside={true}
               closeOnHardwareBackPress={false}
               showCancelButton={false}
               showConfirmButton={true}
               confirmText="Ok"
               confirmButtonColor="#4cb69f"
               onConfirmPressed={() => {
                  this.hideAlert();
               }}
            />

         </ImageBackground>
      )

   }
}

const styles = StyleSheet.create({

   imgBackground: {
      width: '100%',
      height: '100%',
   },
   container: {
      flex: 1,
      paddingHorizontal: 20,
      paddingTop: 30,
      marginTop: 75
   },
   title: {
      fontSize: 50,
      fontWeight: 'bold',
      color: '#000F60',
      textAlign: 'center',
      marginBottom: 50
   },
   input: {
      borderRadius: 4,
      borderWidth: 1,
      borderColor: '#ddd',
      padding: 15,
      marginTop: 10,
      fontSize: 16,
      backgroundColor: "#fff"
   },
   shareButton: {
      backgroundColor: '#4cb69f',
      borderRadius: 15,
      height: 55,
      marginTop: 15,

      justifyContent: 'center',
      alignItems: 'center',
   },
   subButton: {
      fontSize: 20,
      color: '#FFF',
      marginTop: 10,
      textAlign: 'center'
   }
});

import React, { Component } from 'react';
import AsyncStorage from '@react-native-community/async-storage';

import { View, Text, StyleSheet, TextInput, TouchableOpacity, ImageBackground } from 'react-native';

import api from '../../services/api';

export default class RegisterUser extends Component {
   state = {
      name: '',
      email: '',
      pass: ''
   }

   static navigationOptions = ({ navigation }) => {
      return {
         header: () => null
      }
   }

   async setStorage(data) {
      await AsyncStorage.setItem('@storage_Key', JSON.stringify(data))
   }

   handleSubmit = () => {
      api.post('api/user', {
         name: this.state.name,
         email: this.state.email,
         password: this.state.pass
      }).then(res => {
         this.setStorage(res.data)
         this.props.navigation.navigate('ListPet');
      }, err => {
      })
   }

   login = () => {
      this.props.navigation.navigate('LoginUser');
   }

   render() {
      return (
         <ImageBackground
            style={styles.imgBackground}
            source={require('./../../../assets/backgroundLogin.jpg')}>
            <View style={styles.container}>
               <Text style={styles.title}>Registre-se</Text>
               <TextInput
                  style={styles.input}
                  autoCorrect={false}
                  placeholder="Nome completo"
                  placeholderTextColor="#999"
                  value={this.state.name}
                  onChangeText={name => this.setState({ name })}
               />
               <TextInput
                  style={styles.input}
                  autoCorrect={false}
                  placeholder="E-mail"
                  placeholderTextColor="#999"
                  value={this.state.email}
                  autoCapitalize = 'none'
                  onChangeText={email => this.setState({ email })}
               />

               <TextInput
                  style={styles.input}
                  autoCorrect={false}
                  placeholder="Senha"
                  placeholderTextColor="#999"
                  secureTextEntry={true}
                  value={this.state.pass}
                  autoCapitalize = 'none'
                  onChangeText={pass => this.setState({ pass })}
               />

               <TextInput
                  style={styles.input}
                  autoCorrect={false}
                  placeholder="Confirmar senha"
                  placeholderTextColor="#999"
                  secureTextEntry={true}
                  value={this.state.breed}
                  autoCapitalize = 'none'
                  onChangeText={breed => this.setState({ breed })}
               />

               <TouchableOpacity style={styles.shareButton} onPress={this.handleSubmit}>
                  <Text style={styles.sharebuttonText}>Cadastrar</Text>
               </TouchableOpacity>

               <Text style={styles.subButton} onPress={this.login}>Fazer Login</Text>
            </View>
         </ImageBackground>
      )

   }
}

const styles = StyleSheet.create({
   imgBackground: {
      width: '100%',
      height: '100%',
   },
   container: {
      flex: 1,
      paddingHorizontal: 20,
      paddingTop: 30,
   },
   title: {
      fontSize: 40,
      fontWeight: 'bold',
      color: '#000F60',
      textAlign: 'center',
      marginBottom: 30
   },
   input: {
      borderRadius: 4,
      borderWidth: 1,
      borderColor: '#ddd',
      padding: 15,
      marginTop: 10,
      fontSize: 16,
      backgroundColor: '#fff'
   },

   shareButton: {
      backgroundColor: '#4cb69f',
      borderRadius: 15,
      height: 55,
      marginTop: 15,

      justifyContent: 'center',
      alignItems: 'center',
   },
   shareButtonText: {
      fontWeight: 'bold',
      fontSize: 16,
      color: '#FFF',
   },
   subButton: {
      fontSize: 20,
      color: '#FFF',
      marginTop: 10,
      textAlign: 'center'
   }
});
